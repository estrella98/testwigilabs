package com.starcode.framework.retrofit.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GetMoviesResDTO
    (
    val page:Int,
    val results:List<MovieResDTO>,
    @Json(name = "total_pages") val totalPages:Int,
    @Json(name = "total_results") val totalResults:Int,
)

@JsonClass(generateAdapter = true)
data class MovieResDTO
    (
    val id:Int,
    @Json(name = "original_title") val originalTitle:String,
    @Json(name = "overview") val overview:String,
    @Json(name = "popularity") val popularity:Double,
    @Json(name = "poster_path") val posterPath:String,
    @Json(name = "release_date") val releaseDate:String,
    @Json(name = "title") val title:String,
    @Json(name = "vote_average") val voteAverage:Double,
    @Json(name = "vote_count") val voteCount:Int
)