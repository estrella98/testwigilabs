package com.starcode.framework.retrofit

import com.starcode.framework.retrofit.APIConstants.GET_MOVIES
import com.starcode.framework.retrofit.dto.GetMoviesResDTO
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface ApiService {

    @GET(GET_MOVIES)
    fun getMovies(): Single<GetMoviesResDTO>

}