package com.starcode.framework.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.starcode.framework.room.DBConstants
import com.starcode.framework.room.model.MovieEntity
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item:MovieEntity):Maybe<Long>

    @Query("SELECT * FROM ${DBConstants.Movie.TABLE_NAME}")
    fun getAllMovies(): Flowable<List<MovieEntity>>
}