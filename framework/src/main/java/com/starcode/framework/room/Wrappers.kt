package com.starcode.framework.room

import com.starcode.framework.room.model.MovieEntity
import io.reactivex.rxjava3.core.Flowable


data class WrapMovie(
    val id: Int,
    val originalTitle: String,
    val title: String,
    val overview: String,
    val popularity: Double,
    val posterPath: String,
    val releaseDate: String,
    val voteAverage: Double,
    val voteCount: Int
){
    fun toEntity() = MovieEntity(id, originalTitle, title, overview, popularity, posterPath, releaseDate, voteAverage, voteCount)
}
fun Flowable<List<MovieEntity>>.toGetList() : Flowable<List<WrapMovie>> = map { it.map { WrapMovie(it.id, it.originalTitle, it.title, it.overview, it.popularity, it.posterPath, it.releaseDate, it.voteAverage, it.voteCount) }}

