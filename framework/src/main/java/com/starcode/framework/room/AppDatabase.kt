package com.starcode.framework.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.starcode.framework.room.dao.MovieDao
import com.starcode.framework.room.model.MovieEntity

/**
 *To manage data items that can be accessed, updated
 * @Created by ESTRELLA
 */
@Database(
    entities = [MovieEntity::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun movieDao(): MovieDao
}