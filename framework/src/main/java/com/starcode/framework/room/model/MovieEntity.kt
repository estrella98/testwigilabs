package com.starcode.framework.room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.starcode.framework.room.DBConstants

@Entity(tableName = DBConstants.Movie.TABLE_NAME)
data class MovieEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = DBConstants.Movie.COLUMN_ID)
    val id: Int,
    @ColumnInfo(name = DBConstants.Movie.COLUMN_ORIGINAL_TITLE)
    val originalTitle: String,
    @ColumnInfo(name = DBConstants.Movie.COLUMN_TITLE)
    val title: String,
    @ColumnInfo(name = DBConstants.Movie.COLUMN_OVERVIEW)
    val overview: String,
    @ColumnInfo(name = DBConstants.Movie.COLUMN_POPULARITY)
    val popularity: Double,
    @ColumnInfo(name = DBConstants.Movie.COLUMN_POSTER_PATH)
    val posterPath: String,
    @ColumnInfo(name = DBConstants.Movie.COLUMN_RELEASE_DATE)
    val releaseDate: String,
    @ColumnInfo(name = DBConstants.Movie.COLUMN_VOTE_AVERAGE)
    val voteAverage: Double,
    @ColumnInfo(name = DBConstants.Movie.COLUMN_VOTE_COUNT)
    val voteCount: Int
)