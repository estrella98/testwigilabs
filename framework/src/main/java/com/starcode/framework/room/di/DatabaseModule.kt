package com.starcode.framework.room.di

import android.content.Context
import androidx.room.Room
import com.starcode.framework.room.AppDatabase
import com.starcode.framework.room.DBConstants
import com.starcode.framework.room.dao.MovieDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    fun appDatabaseProvider(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, DBConstants.NAME_DB)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun movieDaoProvider(RoomDatabase: AppDatabase): MovieDao {
        return RoomDatabase.movieDao()
    }
}