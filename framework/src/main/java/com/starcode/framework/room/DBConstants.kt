package com.starcode.framework.room

object DBConstants {

    const val NAME_DB = "database_movies"

    object Movie {
        const val TABLE_NAME = "movie"
        const val COLUMN_ID = "id"
        const val COLUMN_ORIGINAL_TITLE= "original_title"
        const val COLUMN_TITLE= "title"
        const val COLUMN_OVERVIEW = "overview"
        const val COLUMN_POPULARITY = "popularity"
        const val COLUMN_POSTER_PATH = "poster_path"
        const val COLUMN_RELEASE_DATE = "release_date"
        const val COLUMN_VOTE_AVERAGE= "vote_average"
        const val COLUMN_VOTE_COUNT = "vote_count"
    }

}