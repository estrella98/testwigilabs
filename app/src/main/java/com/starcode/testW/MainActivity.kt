package com.starcode.testW

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.starcode.movies.presentation.MovieActivity
import com.starcode.testW.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)


        GlobalScope.launch {
            delay(1500)
            runOnUiThread { loadMovies() }
        }
    }

    private fun loadMovies() {
        val intent = Intent(this, MovieActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }
}