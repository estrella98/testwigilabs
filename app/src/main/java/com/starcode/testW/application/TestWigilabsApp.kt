package com.starcode.testW.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TestWigilabsApp : Application() {
}