package com.starcode.movies.domain.model


data class Movie(
    val id: Int,
    val originalTitle: String,
    val title: String,
    val overview: String,
    val popularity: Double,
    var posterPath: String,
    val releaseDate: String,
    val voteAverage: Double,
    val voteCount: Int
)
