package com.starcode.movies.domain.interactor

import com.starcode.movies.domain.model.Movie
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single

interface IHomeRepository {

    fun getMovies(): Single<List<Movie>>
    fun saveMovies(list: List<Movie>):Boolean
    fun getMoviesLocal(): Flowable<List<Movie>>
}