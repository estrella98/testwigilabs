package com.starcode.movies.domain.usescase

import com.starcode.movies.domain.interactor.IHomeRepository
import com.starcode.movies.domain.model.Movie
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetMoviesUseCase @Inject constructor(private val iHomeRepository: IHomeRepository){

    fun invoke(): Single<List<Movie>> = iHomeRepository.getMovies()
}
class SaveMoviesUseCase @Inject constructor(private val iHomeRepository: IHomeRepository){

    fun invoke(list:List<Movie>) = iHomeRepository.saveMovies(list)
}
class GetMoviesLocalUseCase @Inject constructor(private val iHomeRepository: IHomeRepository){

    fun invoke() = iHomeRepository.getMoviesLocal()
}