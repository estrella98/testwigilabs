package com.starcode.movies.data.source.local

import com.starcode.movies.domain.model.Movie
import io.reactivex.rxjava3.core.Flowable

interface IMovieLocalDataSource {

    fun saveMovies(list: List<Movie>):Boolean

    fun getMoviesLocal(): Flowable<List<Movie>>
}