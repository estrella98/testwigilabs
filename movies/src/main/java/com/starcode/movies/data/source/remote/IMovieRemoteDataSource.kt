package com.starcode.movies.data.source.remote

import com.starcode.movies.domain.model.Movie
import io.reactivex.rxjava3.core.Single

interface IMovieRemoteDataSource {

    fun getMovies(): Single<List<Movie>>
}