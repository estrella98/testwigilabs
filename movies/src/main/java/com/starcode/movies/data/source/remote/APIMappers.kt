package com.starcode.movies.data.source.remote

import com.starcode.framework.retrofit.dto.GetMoviesResDTO
import com.starcode.framework.retrofit.dto.MovieResDTO
import com.starcode.movies.domain.model.Movie

fun GetMoviesResDTO.toMovieDomainList(): List<Movie> = results.map(MovieResDTO::toMovieDomain)

fun MovieResDTO.toMovieDomain() = Movie(
    id,
    originalTitle,
    title,
    overview,
    popularity,
    posterPath,
    releaseDate,
    voteAverage,
    voteCount
)
