package com.starcode.movies.data.source.remote

import com.starcode.framework.retrofit.ApiService
import com.starcode.framework.retrofit.dto.GetMoviesResDTO
import com.starcode.movies.domain.model.Movie
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class MovieRemoteDataSource @Inject constructor(private val apiService: ApiService) :
    IMovieRemoteDataSource {

    override fun getMovies(): Single<List<Movie>> =
        apiService.getMovies().map(GetMoviesResDTO::toMovieDomainList)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}