package com.starcode.movies.data.repository

import com.starcode.movies.data.source.local.IMovieLocalDataSource
import com.starcode.movies.data.source.remote.IMovieRemoteDataSource
import com.starcode.movies.domain.interactor.IHomeRepository
import com.starcode.movies.domain.model.Movie
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private var iMovieRemoteDataSource: IMovieRemoteDataSource,
    private val iMovieLocalDataSource: IMovieLocalDataSource
) : IHomeRepository {

    override fun getMovies(): Single<List<Movie>> = iMovieRemoteDataSource.getMovies()

    override fun saveMovies(list: List<Movie>) = iMovieLocalDataSource.saveMovies(list)

    override fun getMoviesLocal(): Flowable<List<Movie>>  = iMovieLocalDataSource.getMoviesLocal()
}