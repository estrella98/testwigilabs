package com.starcode.movies.data.source.local

import com.starcode.framework.room.WrapMovie
import com.starcode.framework.room.dao.MovieDao
import com.starcode.framework.room.model.MovieEntity
import com.starcode.framework.room.toGetList
import com.starcode.movies.domain.model.Movie
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.lang.Exception
import javax.inject.Inject

class MovieLocalDataSource  @Inject constructor(private val movieDao: MovieDao): IMovieLocalDataSource {

    override fun saveMovies(list: List<Movie>) :Boolean{
        return try {
            list.forEach { movieDao.insert(it.toMovieEntity())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe()
            }
            true
        }catch (e:Exception){
            false
        }

    }

    override fun getMoviesLocal(): Flowable<List<Movie>> = movieDao.getAllMovies().toGetList()
        .map(List<WrapMovie>::toMovieDomainList)
        .onErrorReturn { emptyList() }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

}

