package com.starcode.movies.data.source.local

import com.starcode.framework.room.WrapMovie
import com.starcode.framework.room.model.MovieEntity
import com.starcode.movies.domain.model.Movie
import io.reactivex.rxjava3.core.Flowable

fun Movie.toMovieEntity() = WrapMovie(
    id,
    originalTitle,
    title,
    overview,
    popularity,
    posterPath,
    releaseDate,
    voteAverage,
    voteCount
).toEntity()

fun List<WrapMovie>.toMovieDomainList(): List<Movie> = map(WrapMovie::toMovieDomain)

fun WrapMovie.toMovieDomain() = Movie(
    id,
    originalTitle,
    title,
    overview,
    popularity,
    posterPath,
    releaseDate,
    voteAverage,
    voteCount
)