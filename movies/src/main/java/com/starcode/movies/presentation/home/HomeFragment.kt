package com.starcode.movies.presentation.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.starcode.movies.databinding.HomeFragmentBinding
import com.xwray.groupie.GroupieAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private val binding: HomeFragmentBinding by lazy {
        HomeFragmentBinding.inflate(layoutInflater)
    }

    private val viewModel: HomeVM by viewModels()

    private val groupieAdapter = GroupieAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.swipeContainer.isRefreshing = true
        binding.swipeContainer.setOnRefreshListener {
            viewModel.getMovies()
        }
        binding.back.setOnClickListener { activity?.finish() }

        setUpAdapter()
        setUpItemListener()
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.movies.observe(viewLifecycleOwner, {
            binding.swipeContainer.isRefreshing = false
            Log.d("TAG", "LIST SIZE :${it.size}")
            groupieAdapter.clear()
            groupieAdapter.updateAsync(it.map { m ->
                MovieViewHolder(requireContext(),m)
            })
            binding.textEmpty.isVisible = it.isEmpty()
        })
    }

    private fun setUpAdapter() {
        val mGridManager = GridLayoutManager(requireContext(), 2)
        binding.rvMovies.apply {
            layoutManager = mGridManager
            adapter = groupieAdapter
        }
    }

    private fun setUpItemListener() {
        groupieAdapter.setOnItemClickListener { item, _ ->
            if (item is MovieViewHolder) {
                MovieDetailDialogFragment(item.item).show(childFragmentManager, "MD")
            }
        }
    }
}