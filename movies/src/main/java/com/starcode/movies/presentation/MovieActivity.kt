package com.starcode.movies.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.starcode.movies.R
import com.starcode.movies.databinding.MovieActivityBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieActivity : AppCompatActivity() {

    private val binding: MovieActivityBinding by lazy {
        MovieActivityBinding.inflate(layoutInflater)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }
}