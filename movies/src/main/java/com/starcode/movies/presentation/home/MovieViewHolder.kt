package com.starcode.movies.presentation.home

import android.content.Context
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.starcode.movies.R
import com.starcode.movies.databinding.ItemMovieBinding
import com.starcode.movies.domain.model.Movie
import com.xwray.groupie.viewbinding.BindableItem

class MovieViewHolder(val context: Context, val item: Movie) :
    BindableItem<ItemMovieBinding>() {

    override fun bind(viewBinding: ItemMovieBinding, position: Int) {

        viewBinding.apply {

            val options = RequestOptions()
                .placeholder(R.drawable.image)
                .error(R.drawable.image)
            Glide.with(context)
                .load(item.posterPath).apply(options).into(photo)

            title.text = item.originalTitle
            description.text = item.overview
            rating.rating = (item.voteAverage / 2).toFloat()

        }
    }

    override fun getLayout() = R.layout.item_movie

    override fun initializeViewBinding(view: View): ItemMovieBinding =
        ItemMovieBinding.bind(view)
}