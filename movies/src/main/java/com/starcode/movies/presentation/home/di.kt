package com.starcode.movies.presentation.home

import com.starcode.movies.data.repository.HomeRepository
import com.starcode.movies.domain.usescase.GetMoviesLocalUseCase
import com.starcode.movies.domain.usescase.GetMoviesUseCase
import com.starcode.movies.domain.usescase.SaveMoviesUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class HomeFragmentModule{

    @Provides
    fun getMoviesUseCaseProvider(homeRepository: HomeRepository) = GetMoviesUseCase(homeRepository)
    @Provides
    fun saveMoviesUseCaseProvider(homeRepository: HomeRepository) = SaveMoviesUseCase(homeRepository)

    @Provides
    fun getMoviesLocalUseCaseProvider(homeRepository: HomeRepository) = GetMoviesLocalUseCase(homeRepository)
}