package com.starcode.movies.presentation.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.starcode.movies.domain.model.Movie
import com.starcode.movies.domain.usescase.GetMoviesLocalUseCase
import com.starcode.movies.domain.usescase.GetMoviesUseCase
import com.starcode.movies.domain.usescase.SaveMoviesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable
import java.lang.Exception
import java.net.UnknownHostException
import javax.inject.Inject

@HiltViewModel
class HomeVM @Inject constructor(
    private val getMoviesUseCase: GetMoviesUseCase,
    private val saveMoviesUseCase: SaveMoviesUseCase,
    private val getMoviesLocalUseCase: GetMoviesLocalUseCase
) : ViewModel() {

    private val disposable = CompositeDisposable()

    private val _movies = MutableLiveData<List<Movie>>()
    val movies: LiveData<List<Movie>> get() = _movies


    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    init {
        getMovies()

    }
    fun getMovies(){
        disposable.add(getMoviesUseCase.invoke()
            .doOnSubscribe { }
            .subscribe({ movieList ->
                saveMoviesUseCase.invoke(movieList)
                setImages(movieList)

            }, { error ->
                if (error is UnknownHostException) getMoviesLocal()
            })
        )
    }

    private fun setImages(movieList: List<Movie>) {

        if (movieList.isNotEmpty()) {
            movieList[0].posterPath = "https://i.redd.it/9uudt2n6bij61.jpg"
            movieList[1].posterPath =
                "https://upload.wikimedia.org/wikipedia/en/1/18/Venom_%282018_film_poster%29.png"
            movieList[2].posterPath =
                "https://th.bing.com/th/id/OIP.YLOnGJ-U0sh6Q0euxU7d6wHaKj?pid=ImgDet&rs=1"
            movieList[3].posterPath =
                "https://th.bing.com/th/id/R.26f2a100eef879a2e4c4a80d242958a6?rik=pOqnLjkVssxleA&pid=ImgRaw&r=0"
            movieList[4].posterPath =
                "https://th.bing.com/th/id/OIP.B3wUS4WkOtFHmQJyMbD1wAHaKy?pid=ImgDet&rs=1"
            movieList[5].posterPath =
                "https://th.bing.com/th/id/OIP.DwWiM9Uda5fkBE4JkyBTfAHaK-?pid=ImgDet&rs=1"
            movieList[6].posterPath =
                "https://image.tmdb.org/t/p/w1280/ww2YPsGe6Yd1nUHH6XVvfj4Rrin.jpg"
            movieList[7].posterPath =
                "https://unika.fm/wp-content/uploads/2021/07/20210712_encanto.jpg"
            movieList[8].posterPath =
                "https://th.bing.com/th/id/R.a193e43ff7acacfc075d720cde984500?rik=KrYzn46%2bfhjacw&pid=ImgRaw&r=0"
            movieList[9].posterPath =
                "https://th.bing.com/th/id/R.3fff3545e8f52e3694ea6d9361652e0a?rik=%2bvb3csz9Lt5n7w&pid=ImgRaw&r=0"
            movieList[10].posterPath =
                "https://th.bing.com/th/id/R.f7d844f57f154d19fcb0bfe43df62111?rik=5SVnmlZHkRlxIA&pid=ImgRaw&r=0"
            movieList[11].posterPath =
                "https://th.bing.com/th/id/OIP.3_7fXGc6vpyq-PmoclevmAHaLH?pid=ImgDet&rs=1"
            movieList[12].posterPath =
                "https://th.bing.com/th/id/OIP.Rqzc_bniu5stOSZnNSqjnQHaK9?pid=ImgDet&rs=1"
            movieList[13].posterPath =
                "https://th.bing.com/th/id/OIP.y57sVDoLJjJQG7J1nJhJbgHaKa?pid=ImgDet&rs=1"
            movieList[14].posterPath = ""
            movieList[15].posterPath =
                "https://th.bing.com/th/id/OIP.nWxTqKxNL9R9eiUbuMFzkgHaK9?pid=ImgDet&rs=1"
            movieList[16].posterPath =
                "https://th.bing.com/th/id/R.4bd5dbc0755d9474c7e176dcc29e995b?rik=ASju%2fedMve8DfQ&pid=ImgRaw&r=0"
            movieList[17].posterPath =
                "https://th.bing.com/th/id/OIF.YdbuLnSQqLGQp5LLEJgcVQ?pid=ImgDet&rs=1"
            movieList[18].posterPath =
                "https://th.bing.com/th/id/R.2cb2a6650feb9ba63a83aa618677fafd?rik=LSyLdZthJ7paQw&pid=ImgRaw&r=0"
            movieList[19].posterPath =
                "https://image.tmdb.org/t/p/w300/sYoWjGSW4XQRWwFDHf3kdMqtCLr.jpg"
        }
        _movies.value = movieList
    }

    private fun getMoviesLocal() {
        Log.d("TAG", "Desde local...")
        disposable.addAll(getMoviesLocalUseCase.invoke()
            .subscribe { movieList ->
                setImages(movieList)
            })
    }
}