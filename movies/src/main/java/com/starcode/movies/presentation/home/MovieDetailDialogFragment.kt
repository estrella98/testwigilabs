package com.starcode.movies.presentation.home

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.starcode.movies.R
import com.starcode.movies.databinding.MovieDetailDialogBinding
import com.starcode.movies.domain.model.Movie

class MovieDetailDialogFragment(val item: Movie) : DialogFragment(){

    private val binding:MovieDetailDialogBinding by  lazy {
        MovieDetailDialogBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //data
        binding.apply {
            title.text= item.originalTitle
            description.text = item.overview
            rating.text = item.voteAverage.toString()
            date.text = item.releaseDate
            popularity.text = getString(R.string.txt_home_item_popularity, item.popularity.toString())
            vote.text = getString(R.string.txt_home_item_vote, item.voteCount.toString())
            val options = RequestOptions()
                .placeholder(R.drawable.image)
                .error(R.drawable.image)
            Glide.with(requireContext())
                .load(item.posterPath).apply(options).into(photo)
        }
    }


    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            it.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
        }
    }
}