package com.starcode.movies.di

import com.starcode.movies.data.source.local.IMovieLocalDataSource
import com.starcode.movies.data.source.local.MovieLocalDataSource
import com.starcode.movies.data.source.remote.IMovieRemoteDataSource
import com.starcode.movies.data.source.remote.MovieRemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MovieModule {

    @Provides
    @Singleton
    fun movieRemoteDataSourceProvider(movieRemoteDataSource: MovieRemoteDataSource): IMovieRemoteDataSource =
        movieRemoteDataSource


    //locals
    @Provides
    @Singleton
    fun movieLocalDataSourceProvider(movieLocalDataSource: MovieLocalDataSource): IMovieLocalDataSource =
        movieLocalDataSource
}