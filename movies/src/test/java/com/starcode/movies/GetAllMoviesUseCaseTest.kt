package com.starcode.movies

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.starcode.movies.data.repository.HomeRepository
import com.starcode.movies.domain.model.Movie
import com.starcode.movies.domain.usescase.GetMoviesUseCase
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetAllMoviesUseCaseTest {

    @Mock
    private lateinit var homeRepository: HomeRepository

    private lateinit var getMoviesUseCase: GetMoviesUseCase

    @Before
    fun setUp() {
        getMoviesUseCase = GetMoviesUseCase(homeRepository)
    }

    @Test
    fun `get all movies use case should return a list of movies from server`() {
        val expectedResult = listOf(mockedMovie)
        given(homeRepository.getMovies()).willReturn(Single.just(expectedResult))

        getMoviesUseCase.invoke()
            .test()
            .assertComplete()
            .assertNoErrors()
            .assertValueCount(1)
            .assertValue(expectedResult)
    }

    val mockedMovie = Movie(
        1,
        "Spider-Man: No Way Home",
        "Spider-Man: No Way Home",
        "Peter Parker is unmasked and..",
        1532.5,
        "",
        "2021-12-15",
        5.6,
        1562
    )
}